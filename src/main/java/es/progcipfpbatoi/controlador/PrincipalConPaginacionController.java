package es.progcipfpbatoi.controlador;

import es.progcipfpbatoi.exceptions.WrongParameterException;
import es.progcipfpbatoi.modelo.dao.PersonaDAOInterface;
import es.progcipfpbatoi.modelo.dto.Persona;
import es.progcipfpbatoi.util.AlertMessages;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class PrincipalConPaginacionController implements Initializable {

    private final static int INDICE_PAGINA_INICIAL = 0;
    private final static int FILAS_POR_PAGINA = 10;

    @FXML
    private TableColumn<Persona, String> nombreCol;
    @FXML
    private TableColumn<Persona, String> telefonoCol;
    @FXML
    private TableColumn<Persona, Void> botonCol;

    @FXML
    private Hyperlink hlAtras;

    @FXML
    private Hyperlink hlSiguiente;

    @FXML
    private TableView<Persona> tableViewPersonas;

    private PersonaDAOInterface personaDAO;
    private int currentPageIndex = 0;
    private int totalDataToShow;

    public PrincipalConPaginacionController(PersonaDAOInterface personaDAO) {
        this.personaDAO = personaDAO;
    }


    @FXML
    private void handleLinkSiguiente() {
        try {
            nextPage(tableViewPersonas, hlAtras, hlSiguiente);
        } catch (WrongParameterException ex) {
            AlertMessages.mostrarAlertError(ex.getMessage());
        }
    }

    private void nextPage(TableView<Persona> tableView, Hyperlink atras, Hyperlink siguiente) throws WrongParameterException{

        currentPageIndex++;
        showPageWithTransition(tableView, atras, siguiente);
    }

    private void showPageWithTransition(TableView<Persona> tableView, Hyperlink atras, Hyperlink siguiente) {
        FadeTransition fadeOut = createFadeTransition(1.0, 0.0);
        fadeOut.setOnFinished(event -> {
            try {
                showPage(tableView, atras, siguiente);
                FadeTransition fadeIn = createFadeTransition(0.0, 1.0);
                fadeIn.play();
            }catch (WrongParameterException ex) {
                ex.getMessage();
            }
        });
        fadeOut.play();
    }

    private void previousPage(TableView<Persona> tableView, Hyperlink atras, Hyperlink siguiente) throws WrongParameterException {
        if (currentPageIndex > 0) {
            currentPageIndex--;
            showPageWithTransition(tableView, atras, siguiente);
        }
    }

    @FXML
    private void handleLinkAtras() {
        try {
            previousPage(tableViewPersonas, hlAtras, hlSiguiente);
        } catch (WrongParameterException ex) {
            AlertMessages.mostrarAlertError(ex.getMessage());
        }
    }

    private void showPage(TableView<Persona> tableView, Hyperlink atras, Hyperlink siguiente) throws WrongParameterException {
        tableView.getItems().clear();
        List<Persona> pageData = fetchDataForPage();
        tableView.getItems().addAll(pageData);
        updateLinksState(atras, siguiente);
    }


    private List<Persona> fetchDataForPage() throws WrongParameterException {
       int startIndex = currentPageIndex * FILAS_POR_PAGINA;
        int endIndex = Math.min(startIndex + FILAS_POR_PAGINA, totalDataToShow);
        return this.personaDAO.findAll(startIndex, endIndex);
    }

    private void updateLinksState(Hyperlink previousPageButton, Hyperlink nextPageButton) {
        previousPageButton.setDisable(currentPageIndex <= 0);
        int totalPageCount = (int) Math.ceil((double) totalDataToShow / FILAS_POR_PAGINA);
        nextPageButton.setDisable(currentPageIndex >= totalPageCount - 1);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nombreCol.setCellValueFactory(new PropertyValueFactory<Persona, String>("nombre"));
        telefonoCol.setCellValueFactory(new PropertyValueFactory<Persona, String>("telefono"));
        botonCol.setCellFactory(param -> new ButtonsCellController(this));
        totalDataToShow = this.personaDAO.countAll();
        try {
            showPage(tableViewPersonas, hlAtras, hlSiguiente);
        } catch (WrongParameterException ex) {
            AlertMessages.mostrarAlertError(ex.getMessage());
        }
    }

    private FadeTransition createFadeTransition(double fromValue, double toValue) {
        FadeTransition fadeTransition = new FadeTransition(new Duration(1000), tableViewPersonas);
        fadeTransition.setFromValue(fromValue);
        fadeTransition.setToValue(toValue);
        return fadeTransition;
    }
}
