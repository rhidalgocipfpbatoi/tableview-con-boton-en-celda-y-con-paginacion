package es.progcipfpbatoi.modelo.dto;

import java.time.LocalDate;

public class Persona {

    private static int idCounter = 1;

    private int id;
    private String nombre;
    private String telefono;

    public Persona(String nombre, String telefono) {
        this.nombre = nombre;
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
